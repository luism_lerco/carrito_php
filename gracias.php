<?php
include 'global/config.php';
include 'global/conexion.php';
include 'carrito.php';
include 'templates/header.php';

session_destroy();

?>

<div class="container pt-5">
    <div class="row justify-content-center">
        <div class="jumbotron text-center">
            <h3 class="display-4">¡Gracias por tu pago!</h3>
            <p class="lead">En breve tendras el material en tu bandeja de entrada</p>
            <hr class="my-2">
            <p></p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="/" role="button">Regresar al inicio</a>
            </p>
        </div>
    </div>
</div>

<?php 
include 'templates/footer.php';
?>