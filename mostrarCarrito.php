<?php
include 'global/config.php';
include 'carrito.php';
include 'templates/header.php';
?>

<div class="container pt-5 text-center">
    <h3 class="display-4">Listado de productos</h3>
    <p class="lead mb-0">Aquí puedes revisar tus productos agregados </p>
</div>



    <div class="container pt-4">
    <?php if( !empty($_SESSION['CARRITO']) ) {?>
      <div class="row">
        <div class="col-lg-12 p-5 bg-white rounded shadow mb-5">
          <!-- Shopping cart table -->
          
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col" class="border-0 bg-light">
                    <div class="p-2 px-3 text-uppercase">Producto</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">Cantidad</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">Precio</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase">Total</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase"></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    $total = 0;
                    foreach($_SESSION['CARRITO'] as $indice=>$producto){?>
                <tr>
                  <th scope="row" class="border-0">
                    <div class="p-2">
                      <div class="ml-3 d-inline-block align-middle">
                        <h5 class="mb-0"> <a href="#" class="text-dark d-inline-block align-middle"><?php echo $producto['NOMBRE']?></a></h5>
                        <span class="text-muted font-weight-normal font-italic d-block">Código: <?php echo $producto['CODIGO']?></span>
                      </div>
                    </div>
                  </th>
                  <td class="border-0 align-middle"><strong><?php echo $producto['CANTIDAD']?></strong></td>
                  <td class="border-0 align-middle"><strong>$<?php echo $producto['PRECIO']?></strong></td>
                  <td class="border-0 align-middle"><strong>$<?php echo number_format($producto['PRECIO']*$producto['CANTIDAD'],2) ?></strong></td>
                  <td class="border-0 align-middle">
                    <form action="" method="post">
                        
                        <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($producto['ID'], CODE, KEY);?>">    
                        
                        <button type="submit" 
                            name="btnAccion"
                            value="Eliminar"
                            class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> </button>

                    </form>
                  </td>
                </tr>
                <?php 
                $total = $total + ($producto['PRECIO']*$producto['CANTIDAD']);
                }?>
              </tbody>
            </table>   


          </div>
          <!-- End -->
        </div>
      </div>         

      <form action="pagar.php" method="post"> 
        <div class="row py-5 p-4 mb-5 bg-white rounded shadow">
            <div class="col-lg-6">
            <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Correo de contacto</div>
            <div class="p-4">
                <p class="font-italic mb-4">Los productos se enviaran a este correo.</p>
                <div class="input-group mb-4 border rounded-pill p-2">
                <input type="email" class="form-control border-0" id="email" name="email" aria-describedby="emailHelp" placeholder="Escribe tu correo electrónico">    
                </div>
            </div>
            </div>
            <div class="col-lg-6">
            <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Resumen del pedido </div>
            <div class="p-4">
                <p class="font-italic mb-4">Revisa que el total sea el correcto</p>
                <ul class="list-unstyled mb-4">
                <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Total</strong>
                    <h5 class="font-weight-bold">$<?php echo number_format($total,2); ?> MXN</h5>
                </li>
                </ul><button type="submit" name="btnAccion" class="btn btn-dark rounded-pill py-2 btn-block">Proceder al pago</button>
            </div>
            </div>
        </div>
      </form>      
    
      <?php
            }else{
            ?>
            <div class="row py-5">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <strong>¡No hay productos en el carrito!</strong> Te invitamos a <a href="/" class="alert-link">Ir a la tienda</a>.
                </div>
            </div>
                
            </div>
            
            <?php
            }
            ?>   

    </div>  




<?php
        include 'templates/footer.php';
?>