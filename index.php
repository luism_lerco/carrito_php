<?php
include 'global/config.php';
include 'global/conexion.php';
include 'carrito.php';
include 'templates/header.php';
?>

<header>
    <!-- Siders -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>                   
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="http://placehold.it/1600x550" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/1600x550" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="http://placehold.it/1600x550" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>                                   
    </div>
</header>

<!-- Contenido -->
<div class="container">
    <?php if($mensaje !=""){?>
        
            <div class="alert alert-success" role="alert">
                <?php //print_r($_POST);?>
                <?php echo $mensaje ?>
                <strong><a href="mostrarCarrito.php" class="badge badge-success">Ver Carrito</a></strong>
            </div>
    
    <?php }?>

    <div class="row pb-5 mb-4">
    <?php
        $sentencia=$pdo->prepare("SELECT * FROM `productos` ");
        $sentencia->execute();
        $listaProductos=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        //print_r($listaProductos);
    
        foreach($listaProductos as $producto){
        ?> 
        <div class="col-lg-4 col-md-6 my-4 mb-lg-0">
            <!-- Card Producto -->
            <div class="card rounded shadow border-0">
                <div class="card-body p-4"><img src="https://res.cloudinary.com/mhmd/image/upload/v1556485076/shoes-1_gthops.jpg" alt="" class="img-fluid d-block mx-auto mb-3">
                    <h4> <a href="#" class="text-dark"><?php echo $producto['Nombre'];?></a></h4>
                    <h3 class="text-dark">$<?php echo $producto['Precio'];?></h3>
                    <small class="text-muted font-italic">SKU: <?php echo $producto['Codigo'];?></small>
                    <p class="small text-muted font-italic"><?php echo $producto['Descripcion'];?></p>
                        <!--
                        <ul class="list-inline small">
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star text-success"></i></li>
                            <li class="list-inline-item m-0"><i class="fa fa-star-o text-success"></i></li>
                        </ul>
                        -->
                    <form action="" method="post">
                                
                        <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($producto['ID'], CODE, KEY);?>">
                        <input type="hidden" name="codigo" id="codigo" value="<?php echo openssl_encrypt($producto['Codigo'], CODE, KEY);?>">
                        <input type="hidden" name="nombre" id="nombre" value="<?php echo openssl_encrypt($producto['Nombre'], CODE, KEY);?>">
                        <input type="hidden" name="precio" id="precio" value="<?php echo openssl_encrypt($producto['Precio'], CODE, KEY);?>">
                        <input type="hidden" name="cantidad" id="cantidad" value="<?php echo openssl_encrypt(1, CODE, KEY);?>">   
                        <!-- Boton de agregar al carrito -->
                        <button type="submit" 
                        name="btnAccion" 
                        value="Agregar"
                        class="btn btn-info rounded-pill py-2 btn-block">Agregar al Carrito</button>
                    </form>        
                </div>
            </div>
        </div>
        <?php    
            }
        ?>
    </div>
</div>

    <?php
        include 'templates/footer.php';
    ?>
    
      
    