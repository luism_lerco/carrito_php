<?php
include 'global/config.php';
include 'global/conexion.php';
include 'carrito.php';
include 'templates/header.php';
?>

<?php
    if($_POST){
        $total=0;

        $SID = session_id();

        $Correo = $_POST['email'];
        
        foreach($_SESSION['CARRITO'] as $indice=>$producto){

            $titulo_libro = $producto['NOMBRE'];
            
            $total = $total + ($producto['PRECIO']*$producto['CANTIDAD']);
        }

            $sentencia = $pdo->prepare("INSERT INTO `tblventas`(`ID`, `ClaveTransaccion`, 
                                        `PaypalDatos`, `Fecha`, `Correo`, `Total`, `status`)
                                        VALUES (NULL, :ClaveTransaccion, '', NOW(), :Correo, :Total, 'pendiente' ); ");
            $sentencia->bindParam(":ClaveTransaccion", $SID);
            $sentencia->bindParam(":Correo", $Correo);
            $sentencia->bindParam(":Total", $total);
            
            $sentencia->execute();
            //Recuperar Id de la venta
            $idVenta = $pdo->lastInsertId();
            
            

            foreach($_SESSION['CARRITO'] as $indice=>$producto){
                $sentencia = $pdo->prepare("INSERT INTO `tnldetalleventa`(`ID`, `IDVENTA`, 
                                            `IDPRODUCTO`, `PRECIOUNITARIO`, `CANTIDAD`, `DESCARGADO`) 
                                            VALUES (NULL, :IDVENTA, :IDPRODUCTO, :PRECIOUNITARIO, :CANTIDAD, '0');");
                /*$sentencia = $pdo->prepare("INSERT INTO `tnldetalleventa`(`ID`, `IDVENTA`, 
                                            `IDPRODUCTO`, `PRECIOUNITARIO`, `CANTIDAD`, `DESCARGADO`) 
                                            VALUES (NULL, '3', '3', '50', '1', '0');");*/
                $sentencia->bindParam(":IDVENTA", $idVenta);
                $sentencia->bindParam(":IDPRODUCTO", $producto['LLAVE']);
                $sentencia->bindParam(":PRECIOUNITARIO", $producto['PRECIO']);
                $sentencia->bindParam(":CANTIDAD", $producto['CANTIDAD']);
                
                $sentencia->execute();
            }    
            //echo "<h2>".$total."</h2>";

    }
    
?>
    

<div class="container pt-4">
    <div class="row justify-content-center">
        <div class="jumbotron text-center">
            <h1 class="display-3">¡Paso Final!</h1>
            <hr class="my-4">
            <p class="lead">Estas apunto de pagar con Paypal la cantidad de:<br>
                <span class="display-4">
                    $<?php echo number_format($total,2); ?> MXN</span>
            </p>
            <div id="paypal-button-container"></div>
            <p>Los productos podrán ser descargados una vez que se procese el pago</p>
            <p class="lead">
            </p>
        </div>
    </div>
</div>

<?php
$items = '';
foreach($_SESSION['CARRITO'] as $indice=>$producto) {
 $items .= $producto['NOMBRE']. ", ";

}

//echo "<p>".$items."</p>";

?>

<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=MXN"></script>

<script>
        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({

            // Set up the transaction
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '<?php echo $total; ?>',
                            currency: 'MXN'
                        },
                        description:"Material adquirido: <?php echo $items;?>",
                        custom:"<?php echo $SID;?>#<?php echo openssl_encrypt($idVenta, CODE, KEY);?>"
                    }]
                });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    //vemos que fue lo que se esta recibiendo
                    console.log(data);
                    //window.location = "verificador.php?paymentToken="+data.orderID+"&paymentID="+data.payerID;
                    //window.location = "verificador.php?orderID="+data.orderID;
                    window.location = "gracias.php";
                    // Show a success message to the buyer
                    alert('Compra realizada por ' + details.payer.name.given_name + '!');
                });
            }


        }).render('#paypal-button-container');
    </script>



<?php 
include 'templates/footer.php';
?>